# Xiaomi Air 13 (2017) with Solus Tutorial

## Installation

Download ISO from [here](https://solus-project.com/download/)

Choose the version you want. Mate seems to be lighter.

Installation is performed properly with UEFI support.

## HARDWARE

### WIFI

Works perfectly !

### SOUND

Works perfectly !

### BLUETOOTH

I didn't really check.

### KEYBOARD

Backlight works perfectly !

# French Keyboard

For french users, a skin exists [here](https://fr.aliexpress.com/item/Fran-ais-clavier-azerty-Clavier-D-ordinateur-Portable-Protecteur-Pour-Xiaomi-Mi-Portable-Air-12-12/32891098998.html). Thanks to [ShoGuSho](https://forum.hardware.fr/hfr/OrdinateursPortables/Ultraportable/xiaomi-notebook-notebook-sujet_80091_111.htm).
use SHIFT ALTGR W ou X for minus or superior symbols.

### DISPLAY

You can use my ICC profile provided.

### NVIDIA & Intel GPU

Linux Driver Management from Solus doesn't support (yet) the Optimus (possibility to switch easily to GPU we want).

Fisrt, install NVIDIA driver via doflicky.

Then, install [this script](https://github.com/MarechalLima/Solus-Optimus-Switch).

Also, nouveau must be blacklisted to force the use of Intel driver:

Create the blacklist file:

`nano /usr/lib/modprobe.d/blacklist-nouveau.conf`

with the following contents:

`blacklist nouveau`

`options nouveau modeset=0`

### FINGERPRINT

Xiaomi use a fingerprint from elan tech, this model is not yet supported by libfprint but someone work on it, check [here](https://github.com/iafilatov/libfprint).

Install the eopkg packages of libfprint, fprint_demo and fprintd I provide.

Try `sudo fprint_demo` to test the fingerprint

You can also do it via fpintd with `frpintd-enroll` and `fprintd-verify`

**NOT FULLY OPERATIONAL** check also this [link](https://bugs.launchpad.net/ubuntu/+source/libfprint/+bug/1641290)

### BATTERY LIFE

Install TLP with:

`sudo eopkg it tlp`

`sudo tlp start`

`sudo tlp-stat | grep "TLP power save"`

You should got:

`TLP power save = enabled`

Add TLP for every boot:

`sudo systemctl enable tlp`

Use the tlp configuration file for your conf file: `/etc/default/tlp`


